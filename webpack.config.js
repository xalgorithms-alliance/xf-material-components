
const path = require('path')

module.exports = {
  mode: 'production',
  context: path.join(__dirname, '/src'),
  entry: './components.js',
  output: {
    path: path.join(__dirname, './package'),
    filename: 'index.js',
    library: 'xf-material-components',
    libraryTarget: 'commonjs2',
    publicPath: '/build/'
  },
  resolve: {
    alias: {
      react: path.resolve(__dirname, './node_modules/react'),
      'react-dom': path.resolve(__dirname, './node_modules/react-dom')
    }
  },
  externals: {
    react: {
      commonjs: 'react',
      commonjs2: 'react',
      amd: 'React',
      root: 'React'
    },
    'react-dom': {
      commonjs: 'react-dom',
      commonjs2: 'react-dom',
      amd: 'ReactDOM',
      root: 'ReactDOM'
    }
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx|mjs)?$/,
        exclude: /(node_modules)/,
        use: 'babel-loader'
      },
      { test: /\.css$/, use: 'css-loader' }
    ]
  },
  plugins: []
}
