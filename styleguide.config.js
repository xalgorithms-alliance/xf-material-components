const path = require('path')

module.exports = {
  styleguideComponents: {
    Wrapper: path.join(__dirname, 'src/Provider.js')
  },
  template: {
    head: {
      links: [
        {
          rel: 'stylesheet',
          href: 'https://fonts.googleapis.com/css2?family=Public+Sans&display=swap'
        }
      ]
    }
  },
  theme: {
    fontFamily: {
      base: '"Public Sans", sans-serif'
    }
  },
  sections: [
    {
      name: 'Color',
      content: 'src/components/color/color.md'
    },
    {
      name: 'Type',
      content: 'src/components/type/type.md',
      components: 'src/components/type/CodeArea.jsx'
    },
    {
      name: 'Icons',
      components: 'src/components/Icons/*.jsx',
      ignore: 'src/components/Icons/iconlib.js'
    },
    {
      name: 'Buttons',
      components: 'src/components/buttons/*.jsx'
    },
    {
      name: 'Inputs',
      components: 'src/components/inputs/*.jsx'
    }
  ]
}
