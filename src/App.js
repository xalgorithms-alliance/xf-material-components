import React from 'react'
import './config/rootStyle.css'

function App () {
  return (
    <div className="App">
      <h1>Hello World</h1>
    </div>
  )
}

export default App
