import React from 'react'
import { Link } from 'react-router-dom'

function Navbar () {
  const listyle = {
    display: 'flex',
    alignItems: 'center'
  }

  const hold = {
    width: '100%',
    padding: '1em',
    background: 'var(--slate)',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center'
  }

  const small = {
    color: 'white',
    textDecoration: 'none',
    fontSize: '1.6em'
  }

  const linkStyle = {
    textDecoration: 'none',
    color: 'white',
    marginRight: '1em'
  }

  return (
    <div style={hold}>
        <div style={listyle}>
            <Link to="/" style={linkStyle}><span style={small}>XF</span></Link>
            <Link to="/" style={linkStyle}>Design and Brand</Link>
        </div>
        <ul style={listyle}>
            <li><Link to="/brand" style={linkStyle}>Brand</Link></li>
            <li><Link to="/writing" style={linkStyle}>Writing</Link></li>
            <li><Link to="/components" style={linkStyle}>Components</Link></li>
            <li><Link to="/contribute" style={linkStyle}>Contribute</Link></li>
        </ul>
    </div>
  )
}

export default Navbar
