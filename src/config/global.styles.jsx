import { createGlobalStyle } from 'styled-components'
import './style.css'

const GlobalStyle = createGlobalStyle`
* {
  box-sizing: border-box;
  margin: 0;
  padding: 0;
}

body, html {
  font-family: 'PublicSans';
  font-size: 16px;
  color: var(--slate)
}

:root {
  --naval: #062AA7;
  --primary: #204EF0;
  --firmament: #DBEAFF;
  --clear: #EDF5FF;

  --slate: #1E2033;
  --steel: #39435B;
  --frame: #E7EAEE;

  --royal: #372989;
  --orchid: #DBD7F8;
  --satin: #F1EEFC;
  --gleam: #F9FBFE;

  --error: #B44C3E;
  --coral: #ED9C91;
  --valentine: #F6CDC7;
  --smartie: #FAE5E2;

  --groto: #6C8F73;
  --clover: #9CD1A8;
  --lichen: #BFDFC6;
  --seafoam: #E4F2E7;

  --zest: #F2ECB7;

}

h1 {
  font-size: 3.4em;
}

h2 {
  font-size: 2.8em;
}

h3 {
  font-size: 2.2em;
}

h4 {
  font-size: 1.6em;
}

h5 {
  font-size: 1em;
}

@media screen and (max-width: 900px) {
  body, html {
    font-size: 14px;
  }
}

.contentHold>* {
  margin-bottom: 1em;
}

p {
  font-size: 1em;
  color: var(--steel);
  font-weight: 200;
  line-height: 140%;
}

code {
  font-family: 'SpaceMono';
  color: var(--steel);
  background-color: var(--frame);
  font-size: 0.8em;
  padding: 0.1em 1em 0.1em 1em;
  border-radius: 0.2em;
}

.baskerville {
  font-family: 'LibreBaskerville';
  font-size: 0.9em;
}

.baskervilleHeadline {
  font-family: 'LibreBaskerville';
  font-size: 2em;
}

kbd {
  font-family: 'SpaceMono';
  color: var(--primary);
  background-color: var(--firmament);
  font-size: 0.8em;
  padding: 0.1em 1em 0.1em 1em;
  border-radius: 0.2em;
}

mark {
  color: var(--royal);
  background-color: var(--orchid);
  padding: 0.1em 1em 0.1em 1em;
  border-radius: 0.2em;
  font-weight: 200;
  font-size: 0.9em;
}

a {
  color: var(--primary);
  text-decoration: underline;
  font-weight: 200;
}

.formSmall {
  text-transform: uppercase;
  font-size: 0.75em;
}

.errorSmall {
  text-transform: uppercase;
  font-size: 0.75em;
  color: var(--coral);
  margin-top: 0.5em;
}

pre[class*="language-"],
:not(pre) > code[class*="language-"] {
    background: hsl(30, 20%, 25%);
}

/* layout */
.column {
  margin-bottom: 1em;
}

.column>* {
  margin-top: 1em;
}

.grid12 {
  display: grid;
  width: 100%;
  grid-template-columns: 1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr;
}

/* Code blocks */
pre[class*="language-"] {
    padding: 1em;
    margin: .5em 0;
    overflow: auto;
    border: .3em solid hsl(30, 20%, 40%);
    border-radius: .5em;
    box-shadow: 1px 1px .5em black inset;
}

/* Inline code */
:not(pre) > code[class*="language-"] {
    border-radius: .3em;
    border: .13em solid hsl(30, 20%, 40%);
    box-shadow: 1px 1px .3em -.1em black inset;
    white-space: normal;
    padding: 1em;
}

.token.comment,
.token.prolog,
.token.doctype,
.token.cdata {
    color: #ACACAC;
}

.token.punctuation {
    opacity: .7;
}

.token.namespace {
    opacity: .7;
}

.token.property,
.token.tag,
.token.boolean,
.token.number,
.token.constant,
.token.symbol {
    color: var(--steel);
}

.token.selector,
.token.attr-name,
.token.string,
.token.char,
.token.builtin,
.token.inserted {
    color: var(--primary);
}

.token.operator,
.token.entity,
.token.url,
.language-css .token.string,
.style .token.string,
.token.variable {
    color: var(--groto);
}

.token.atrule,
.token.attr-value,
.token.keyword {
    color: var(--royal);
}

.token.regex,
.token.important {
    color: var(--groto);
}

.token.important,
.token.bold {
    font-weight: bold;
}
.token.italic {
    font-style: italic;
}

.token.entity {
    cursor: help;
}

.token.deleted {
    color: red;
}

.linkButtonHold {
  display: flex;
  animation-name: nudgeout;
  animation-duration: 2s;
  animation-fill-mode: both;
}

.linkButtonHold:hover > p {
  animation-name: nudgein;
  animation-duration: 1.2s;
  animation-iteration-count: infinite;
  animation-fill-mode: both;
  padding-right: 0em;
}

@keyframes nudgein {
  0% {
    padding-right: 0em;
  }
  50% {
    padding-right: 1em;
  }
  100% {
    padding-right: 0em;
  }
}

@keyframes nudgeout {
  0% {
    padding-right: 0em;
  }
  50% {
    padding-right: 1em;
  }
  100% {
    padding-right: 0em;
  }
}
`

export default GlobalStyle
