/* Style */
// export { default as GlobalStyle } from './config/global.styles.jsx'
/* Type */
export { default as CodeArea } from './components/type/CodeArea.jsx'
/* Buttons */
export { default as Button } from './components/buttons/Button.jsx'
export { default as LinkButton } from './components/buttons/LinkButton.jsx'
export { default as Tabs } from './components/buttons/Tabs.jsx'
export { default as TFB } from './components/buttons/TFB.jsx'
/* Icons */
export { default as Icon } from './components/Icons/Icon.jsx'
/* Input */
export { default as InputCheck } from './components/inputs/InputCheck.jsx'
export { default as InputDate } from './components/inputs/InputDate.jsx'
export { default as InputDropdown } from './components/inputs/InputDropdown.jsx'
export { default as InputSlider } from './components/inputs/InputSlider.jsx'
export { default as InputText } from './components/inputs/InputText.jsx'
export { default as TextArea } from './components/inputs/TextArea.jsx'
/* Label */
export { default as Label } from './components/inputs/Label.jsx'
/* Layout */
export { default as Column } from './components/layout/Column.jsx'
export { default as Grid12 } from './components/layout/Grid12.jsx'
