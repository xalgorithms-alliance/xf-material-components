import React from 'react'
import PropTypes from 'prop-types'

function Frame ({ children }) {
  return (
        <div>
            {children}
        </div>
  )
}

Frame.propTypes = {
  children: PropTypes.any
}

export default Frame
