import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const Select = styled.select`
    -webkit-appearance: none;
    -moz-appearance: 'none';
    border: 1px solid;
    border-color: var(--frame);
    padding: 0.5em;
    border-radius: 0.5em;
    font-size: 1em;
    box-shadow: none;
    font-family: PublicSans;
    font-weight: 200;
    color: var(--steel);
    width: 100%;
    background: url('public/dropdown.svg') no-repeat right center;
    transition: background .3s, border .3s, box-shadow .2s;
    ::placeholder {
        color: var(--steel);
    }
    :focus {
        outline: 0;
        border-color: var(--primary);
        box-shadow: 0 0 0 2px var(--firmament);
    }
    &:hover {
      -webkit-appearance: none;
      border: 1px solid var(--primary);
      outline: none;
    }
    `

function InputDropdown ({ value, placeholder, onChange, options = [] }) {
  const renderOptions = () => {
    return options.map(({ value, label, disabled }, index) => (
          <option value={value} key={index}>
            {label}
          </option>
    ))
  }

  return (
        <Select
            value={value}
            placeholder={placeholder}
            onChange={onChange}
        >
            {renderOptions()}
        </Select>
  )
}

InputDropdown.propTypes = {
  value: PropTypes.string,
  placeholder: PropTypes.string,
  onChange: PropTypes.func,
  options: PropTypes.array
}

export default InputDropdown
