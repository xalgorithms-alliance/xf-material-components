import React from 'react'
import PropTypes from 'prop-types'
import Button from '../buttons/Button.jsx'
import Icon from '../Icons/Icon.jsx'

function Label ({ label, contentTitle, content }) {
  const [isOpen, setIsOpen] = React.useState(false)

  const flexy = {
    display: 'flex',
    background: 'var(--primary00)',
    borderRadius: '0.5em',
    padding: '0.5em',
    marginBottom: '1em',
    transition: 'transform 1s'
  }

  const hide = {
    display: 'none',
    transition: 'transform 1s'
  }

  const labelhold = {
    display: 'flex'
  }

  const heading = {
    display: 'flex',
    justifyContent: 'space-between'
  }

  const contenthold = {
    marginLeft: '1em',
    borderLeft: '1px solid var(--primary75)',
    paddingLeft: '1em',
    width: '100%',
    color: 'var(--primary75)'
  }

  const blue = {
    color: 'var(--primary75)'
  }

  const rightmargin = {
    marginRight: '0.5em'
  }

  return (
      <>
        <div style={ isOpen ? (flexy) : (hide)} >
              <svg width="48" height="39" viewBox="0 0 48 39" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M21.5029 11.5876L21.4297 11.9001C21.1302 12.0206 20.891 12.1036 20.7119 12.1492C20.5329 12.198 20.3799 12.2224 20.2529 12.2224C19.9925 12.2224 19.7972 12.1557 19.667 12.0222C19.5368 11.8855 19.4717 11.7179 19.4717 11.5193C19.4717 11.4444 19.4782 11.3663 19.4912 11.2849C19.5042 11.2035 19.5254 11.101 19.5547 10.9773L20.0723 8.9021C20.0983 8.78817 20.1227 8.6661 20.1455 8.53589C20.1715 8.40568 20.1846 8.295 20.1846 8.20386C20.1846 8.02157 20.1536 7.90112 20.0918 7.84253C20.0332 7.78068 19.9111 7.74976 19.7256 7.74976C19.654 7.74976 19.5579 7.76115 19.4375 7.78394C19.3203 7.80672 19.2308 7.82625 19.1689 7.84253L19.2422 7.53003C19.4961 7.4161 19.724 7.33146 19.9258 7.27612C20.1276 7.22078 20.2871 7.19312 20.4043 7.19312C20.6712 7.19312 20.8649 7.25659 20.9854 7.38354C21.1058 7.5105 21.166 7.6814 21.166 7.89624C21.166 7.95483 21.1595 8.03459 21.1465 8.1355C21.1335 8.23641 21.1139 8.33732 21.0879 8.43823L20.5654 10.5134C20.5329 10.6404 20.5036 10.7657 20.4775 10.8894C20.4548 11.0098 20.4434 11.1091 20.4434 11.1873C20.4434 11.3728 20.484 11.4998 20.5654 11.5681C20.6501 11.6365 20.7852 11.6707 20.9707 11.6707C21.0326 11.6707 21.1204 11.6625 21.2344 11.6462C21.3516 11.63 21.4411 11.6104 21.5029 11.5876ZM21.8496 5.34741C21.8496 5.53296 21.7894 5.69572 21.6689 5.83569C21.5485 5.97241 21.3988 6.04077 21.2197 6.04077C21.0537 6.04077 20.9105 5.97567 20.79 5.84546C20.6696 5.712 20.6094 5.56063 20.6094 5.39136C20.6094 5.21232 20.6696 5.05607 20.79 4.92261C20.9105 4.78914 21.0537 4.72241 21.2197 4.72241C21.4053 4.72241 21.5566 4.78589 21.6738 4.91284C21.791 5.03654 21.8496 5.1814 21.8496 5.34741Z" fill="#345FF9"/>
                  <path d="M14.25 1.49487H46C46.8284 1.49487 47.5 2.16645 47.5 2.99487V23.3074C47.5 24.1358 46.8284 24.8074 46 24.8074H42.9375C42.1091 24.8074 41.4375 25.4789 41.4375 26.3074V29.7868C41.4375 30.2062 40.9521 30.4393 40.6248 30.1769L34.6109 25.3567C34.1674 25.0011 33.6159 24.8074 33.0474 24.8074H14.25C13.4216 24.8074 12.75 24.1358 12.75 23.3074V2.99487C12.75 2.16645 13.4216 1.49487 14.25 1.49487Z" stroke="#0258FF"/>
                  <path d="M26.5625 7.10925H42.5625" stroke="#0258FF"/>
                  <path d="M26.5625 11.2343H42.5625" stroke="#0258FF"/>
                  <path d="M17.6875 15.8593L42.5625 15.8593" stroke="#0258FF"/>
                  <path d="M17.6875 20.1093H33" stroke="#0258FF"/>
                  <path d="M28.5625 26.6824V30.3384C28.5625 31.4429 27.6671 32.3384 26.5625 32.3384H15.854C15.3992 32.3384 14.958 32.4934 14.6031 32.7778L8.58929 37.5981C7.93467 38.1228 6.96387 37.6567 6.96387 36.8178V33.3384C6.96387 32.7861 6.51615 32.3384 5.96387 32.3384H2.90137C1.7968 32.3384 0.901367 31.4429 0.901367 30.3384V18.4324C0.901367 17.3278 1.7968 16.4324 2.90137 16.4324H9.0625" stroke="#0258FF"/>
              </svg>
              <div style={contenthold}>
                  <div style={heading}>
                      <p><b style={blue}>{contentTitle}</b></p>
                      <Button type="invisible" onClick={() => setIsOpen(false)}><Icon fill="var(--primary)" name="ex"/></Button>
                  </div>
                  <p>{content}</p>
              </div>
          </div>
        <div style={labelhold}>
            <p style={rightmargin}>{label}</p>
            <Button type="invisible" onClick={() => setIsOpen(true)}><Icon fill="var(--primary)" name="Info" /></Button>
        </div>
      </>
  )
}

Label.propTypes = {
  label: PropTypes.string,
  contentTitle: PropTypes.string,
  content: PropTypes.string
}

export default Label
