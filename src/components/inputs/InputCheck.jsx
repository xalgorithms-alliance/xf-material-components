import React from 'react'
import styled from 'styled-components'

const Checkbox = styled.input`
        -webkit-appearance: none;
        -moz-appearance: 'none';
        height: 1.5em;
        width: 1.5em;
        background: white;
        border: 1px solid;
        border-color: var(--frame);
        border-radius: 0.5em;
        transition: background .3s, border .3s, box-shadow .2s;
        :hover {
            -webkit-appearance: none;
            border: 1px solid var(--firmament);
            outline: none;
        }
        :focus {
            -webkit-appearance: none;
            border: 1px solid var(--primary);
            box-shadow: 0 0 0 2px var(--firmament);
            outline: none;
        }
        :checked {
            background-image: url("public/check.png");
            background-position: center;
            background-size: cover;
            background-color: var(--firmament);
        }
        `

function InputCheck () {
  return (
        <Checkbox type="checkbox"/>
  )
}

export default InputCheck
