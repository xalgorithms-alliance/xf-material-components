import React, { useRef, useEffect } from 'react'
import PropTypes from 'prop-types'

function TFB ({ children, kind }) {
  const naval = {
    padding: '0.5em',
    background: 'var(--naval)',
    color: 'var(--firmament)',
    borderRadius: '0.5em',
    width: '2em',
    height: '2em',
    fontFamily: 'spaceMono',
    textAlign: 'center',
    lineHeight: '1em'
  }

  const primary = {
    padding: '0.5em',
    background: 'var(--primary)',
    color: 'var(--firmament)',
    borderRadius: '0.5em',
    width: '2em',
    height: '2em',
    fontFamily: 'spaceMono',
    textAlign: 'center',
    lineHeight: '1em'
  }

  const firmament = {
    padding: '0.5em',
    background: 'var(--firmament)',
    color: 'var(--primary)',
    borderRadius: '0.5em',
    width: '2em',
    height: '2em',
    fontFamily: 'spaceMono',
    textAlign: 'center',
    lineHeight: '1em'
  }

  const [badge, setBadge] = React.useState(naval)

  function pickstyle (ref) {
    useEffect(() => {
      if (kind === 'T') {
        setBadge(naval)
      } else if (kind === 'B') {
        setBadge(primary)
      } else if (kind === 'F') {
        setBadge(firmament)
      } else {
        setBadge(naval)
      }
    }, [ref])
  }
  const wrapperRef = useRef(null)
  pickstyle(wrapperRef)
  return (
      <div ref={wrapperRef}>
        <div style={badge}>
            {children}
        </div>
      </div>
  )
}

TFB.propTypes = {
  children: PropTypes.string,
  kind: PropTypes.string
}

export default TFB
