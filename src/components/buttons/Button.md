```jsx
const flexhold = {
    display: 'flex',
    width: '100%',
    paddingBottom: '1em'
};

const margin = {
    marginRight: '1em'
};
<>
    <div style={flexhold}>
        <div style={margin}>
            <Button>Hello World</Button>
        </div>
        <div style={margin}>
            <Button type="clear">Hello World</Button>
        </div>
        <div style={margin}>
            <Button type="disabled">Hello World</Button>
        </div>
    </div>
    <Button type="wide">Hello World</Button>
</>
```