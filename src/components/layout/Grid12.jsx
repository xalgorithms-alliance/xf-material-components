import React from 'react'
import PropTypes from 'prop-types'

function Grid12 ({ children }) {
  return (
        <div className="grid12">
            {children}
        </div>
  )
}

Grid12.propTypes = {
  children: PropTypes.any
}

export default Grid12
