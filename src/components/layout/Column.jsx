import React from 'react'
import PropTypes from 'prop-types'

function Column ({ children }) {
  return (
        <div className="column">
            {children}
        </div>
  )
}

Column.propTypes = {
  children: PropTypes.any
}

export default Column
