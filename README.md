# UI Components for the Xalgorithms Foundation

```jsx
$ yarn add xf-material-components
```

[published on npm](https://www.npmjs.com/package/xf-material-components)

## Use with React

These components are ready to go out of the box for applications using react. In the root app.jsx file, import the global style and add the component within the render as follows.

```jsx
// App.js

import { Button, InputText, InputDate } from 'xf-material-components'
import GlobalStyle from 'xf-material-components/config/global.styles'

function App() {
  return (
    <div className="App">
      <GlobalStyle />
      <header className="App-header">
        <Button>Test</Button>
        <InputText />
        <InputDate />
      </header>
    </div>
  );
}

export default App;

```

## Use with Next.js

For react next.js projects, import the `rootStyle.css` into your index.js file.

```jsx
// index.js

import 'xf-material-components/config/rootStyle.css'

```

